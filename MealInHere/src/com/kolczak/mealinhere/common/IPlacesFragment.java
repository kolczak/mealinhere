package com.kolczak.mealinhere.common;

import java.util.List;

public interface IPlacesFragment {
	void refresh(List<Place> data);
	void showProgress();
}
