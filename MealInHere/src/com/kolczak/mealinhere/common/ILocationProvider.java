package com.kolczak.mealinhere.common;
import android.location.Location;


public interface ILocationProvider {
	Location getCurrentLocation();
}
