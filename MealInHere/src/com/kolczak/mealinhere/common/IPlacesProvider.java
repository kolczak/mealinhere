package com.kolczak.mealinhere.common;

import java.util.List;

public interface IPlacesProvider {
	List<Place> getPlaces();
}
