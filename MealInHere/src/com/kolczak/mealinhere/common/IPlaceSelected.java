package com.kolczak.mealinhere.common;

public interface IPlaceSelected {
	void onPlaceSelected(Place place);
}
