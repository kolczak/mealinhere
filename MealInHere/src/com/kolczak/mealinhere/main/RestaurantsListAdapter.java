package com.kolczak.mealinhere.main;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kolczak.mealinhere.R;
import com.kolczak.mealinhere.common.IPlaceSelected;
import com.kolczak.mealinhere.common.Place;

public class RestaurantsListAdapter extends BaseAdapter {

	private List<Place> placesList;
	private Activity activity;

	RestaurantsListAdapter(Activity activity, List<Place> placesList) {
		this.activity = activity;
		this.placesList = placesList;
	}

	@Override
	public int getCount() {
		return this.placesList.size();
	}

	@Override
	public Object getItem(int position) {
		return this.placesList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		ViewHolder viewHolder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(this.activity).inflate(
					R.layout.restaurants_list_row, parent, false);
			viewHolder = createViewHolder(convertView);
		}

		if (viewHolder == null)
			viewHolder = (ViewHolder) convertView.getTag();

		viewHolder.title.setText(this.placesList.get(position).getName());
		convertView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (RestaurantsListAdapter.this.activity instanceof IPlaceSelected)
					((IPlaceSelected) activity)
							.onPlaceSelected(RestaurantsListAdapter.this.placesList
									.get(position));
			}
		});

		return convertView;
	}

	private ViewHolder createViewHolder(View convertView) {
		ViewHolder viewHolder = new ViewHolder();
		viewHolder.title = (TextView) convertView
				.findViewById(R.id.restaurants_list_row_title);
		convertView.setTag(viewHolder);
		return viewHolder;
	}

	private class ViewHolder {
		public TextView title;
	}

	public void refill(List<Place> placesList) {
		this.placesList.clear();
		this.placesList.addAll(placesList);
		notifyDataSetChanged();
	}
}
