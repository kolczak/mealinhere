package com.kolczak.mealinhere.main;

import java.util.Locale;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kolczak.mealinhere.R;

public class SectionsPagerAdapter extends FragmentPagerAdapter {

	private Context context;

	public SectionsPagerAdapter(FragmentManager fragmentManager, Context context) {
		super(fragmentManager);
		this.context = context;
	}

	@Override
	public Fragment getItem(int position) {

		Fragment fragment = null;

		switch (position) {
		case FragmentsPositions.LIST_FRAGMENT:
			fragment = new RestaurantsListFragment();
			break;
		case FragmentsPositions.MAP_FRAGMENT:
			fragment = new RestaurantsMapsFragment();
			break;
		}
		return fragment;
	}

	@Override
	public int getCount() {
		return FragmentsPositions.getCount();
	}

	@Override
	public CharSequence getPageTitle(int position) {
		Locale l = Locale.getDefault();
		switch (position) {
		case FragmentsPositions.LIST_FRAGMENT:
			return this.context.getString(R.string.list).toUpperCase(l);
		case FragmentsPositions.MAP_FRAGMENT:
			return this.context.getString(R.string.map).toUpperCase(l);
		}
		return null;
	}

	public static class FragmentsPositions {
		public final static int LIST_FRAGMENT = 0;
		public final static int MAP_FRAGMENT = 1;

		public static int getCount() {
			return 2;
		}
	}
}
