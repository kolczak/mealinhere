package com.kolczak.mealinhere.main;

import java.util.List;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.kolczak.mealinhere.R;
import com.kolczak.mealinhere.common.ILocationProvider;
import com.kolczak.mealinhere.common.IPlaceSelected;
import com.kolczak.mealinhere.common.IPlacesFragment;
import com.kolczak.mealinhere.common.IPlacesProvider;
import com.kolczak.mealinhere.common.Place;
import com.kolczak.mealinhere.database.PlacesDataSource;
import com.kolczak.mealinhere.dataexchange.BasicDataLoader;
import com.kolczak.mealinhere.dataexchange.Connectivity;
import com.kolczak.mealinhere.dataexchange.IResponseHandler;
import com.kolczak.mealinhere.dataexchange.PlacesLoader;

public class RestaurantsActivity extends FragmentActivity implements
		ActionBar.TabListener, IResponseHandler, LocationListener,
		IPlacesProvider, IPlaceSelected, ILocationProvider {

	private static final String PREFS_NAME = "LocationSettings";
	private static final String LONGITUDE = "longitude";
	private static final String LATITUDE = "latitude";
	private static final int TWO_MINUTES = 1000 * 60 * 2;

	SectionsPagerAdapter sectionsPagerAdapter;
	ViewPager viewPager;
	private LocationManager locationManager = null;
	private String provider = null;
	private List<Place> placesLatelyLoaded;
	private PlacesDataSource placesDataSource;
	private Location currentLocation = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.restaurants_activity);
		
		initializeTabs();

		this.placesDataSource = new PlacesDataSource(this);
		this.placesDataSource.open();
	}

	private void initializeTabs() {

		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		this.sectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager(), this);

		this.viewPager = (ViewPager) findViewById(R.id.restaurants_activity_pager);
		this.viewPager.setAdapter(this.sectionsPagerAdapter);

		this.viewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		for (int i = 0; i < this.sectionsPagerAdapter.getCount(); i++) {
			actionBar.addTab(actionBar.newTab()
					.setText(this.sectionsPagerAdapter.getPageTitle(i))
					.setTabListener(this));
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
		this.placesDataSource.open();
		if (isGPSEnabled() && this.locationManager != null)
			this.locationManager.requestLocationUpdates(this.provider, 400, 1, this);
		else
			initalizeGPS();
	}

	@Override
	protected void onPause() {
		super.onPause();
		this.placesDataSource.close();
		if (this.locationManager != null)
			this.locationManager.removeUpdates(this);
	}

	private void initalizeGPS() {
		if (!isGPSEnabled()) {
			showGPSoffAlertDialog();
		} else {
			this.locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			Criteria criteria = new Criteria();
			criteria.setPowerRequirement(Criteria.POWER_MEDIUM);
			this.provider = this.locationManager.getBestProvider(criteria,
					false);
			Location location = this.locationManager
					.getLastKnownLocation(this.provider);
			this.locationManager.requestLocationUpdates(this.provider, 400, 1, this);
			if (location != null) {
				onLocationChanged(location);
			} else {
				Log.i(getPackageName(), "no location available");
			}
		}
	}

	private boolean isGPSEnabled() {
		LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
		return service.isProviderEnabled(LocationManager.GPS_PROVIDER);
	}

	private void showGPSoffAlertDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.gps_disabled)
				.setMessage(getString(R.string.this_application_needs_gps))
				.setCancelable(false)
				.setPositiveButton(getString(R.string.settings),
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								Intent intent = new Intent(
										Settings.ACTION_LOCATION_SOURCE_SETTINGS);
								startActivity(intent);
							}
						})
				.setNegativeButton(getString(R.string.exit),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
							}
						});

		AlertDialog alert = builder.create();
		alert.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.restaurants, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_refresh:
			refreshData();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void refreshData() {
		if (this.currentLocation != null)
			getPlaces(this.currentLocation.getLatitude(),
					this.currentLocation.getLongitude());
		else
			Toast.makeText(this, getString(R.string.waiting_for_gps_location),
					Toast.LENGTH_SHORT).show();
	}

	private void getPlaces(double latitude, double longitude) {
		if (Connectivity.getConnectivityStatus(this) != Connectivity.TYPE_NOT_CONNECTED) {
			Fragment fragment = findFragmentByPosition(this.viewPager
					.getCurrentItem());

			if (fragment instanceof IPlacesFragment)
				((IPlacesFragment) fragment).showProgress();
			new PlacesLoader(this, latitude, longitude).execute();
		} else {
			showWorkingOfflineBar();
		}
	}

	private void showWorkingOfflineBar() {
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		this.viewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onDataExchanged(BasicDataLoader dataLoader) {
		if (dataLoader instanceof PlacesLoader) {
			onPlacesListLoaded(((PlacesLoader) dataLoader).getResults());
		}
	}

	private void onPlacesListLoaded(List<Place> placesList) {
		if (placesList != null) {
			this.placesLatelyLoaded = placesList;
			this.placesDataSource.setAllPlaces(placesList);

			refreshFragments(placesList);
		} else
			Log.d("RestaurantActivity", "retrieved empty list from Google API");
	}

	private void refreshFragments(List<Place> placesList) {

		for (int i = 0; i < this.sectionsPagerAdapter.getCount(); i++) {
			Fragment fragment = findFragmentByPosition(i);

			if (fragment instanceof IPlacesFragment)
				((IPlacesFragment) fragment).refresh(placesList);
		}

	}

	public Fragment findFragmentByPosition(int position) {
		return getSupportFragmentManager().findFragmentByTag(
				"android:switcher:" + this.viewPager.getId() + ":"
						+ this.sectionsPagerAdapter.getItemId(position));
	}

	@Override
	public void onDataExchangedError(BasicDataLoader dataLoader) {
		Log.d("RestaurantActivity", "error getting data from Google API");
	}

	@Override
	public void onLocationChanged(Location location) {

		if (isBetterLocation(location, getCurrentLocation())) {
			getPlaces(location.getLatitude(), location.getLongitude());
			setCurrentLocation(location);
			Fragment fragment = findFragmentByPosition(SectionsPagerAdapter.FragmentsPositions.MAP_FRAGMENT);
			if (fragment instanceof RestaurantsMapsFragment)
				((RestaurantsMapsFragment) fragment).showLocation(location,
						false);
		}
	}

	@Override
	public void onProviderDisabled(String provider) {
		Log.i(getPackageName(), "disabled new provider: " + provider);
	}

	@Override
	public void onProviderEnabled(String provider) {
		Log.i(getPackageName(), "enabled new provider: " + provider);
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	@Override
	public List<Place> getPlaces() {
		if (this.placesLatelyLoaded == null)
			this.placesLatelyLoaded = loadLatelyLoadedPlacesFromDB();
		return this.placesLatelyLoaded;
	}

	private List<Place> loadLatelyLoadedPlacesFromDB() {
		return this.placesDataSource.getAllPlaces();
	}

	@Override
	public void onPlaceSelected(Place place) {
		this.viewPager.setCurrentItem(
				SectionsPagerAdapter.FragmentsPositions.MAP_FRAGMENT, true);
		Fragment mapFragment = findFragmentByPosition(SectionsPagerAdapter.FragmentsPositions.MAP_FRAGMENT);
		if (mapFragment instanceof RestaurantsMapsFragment)
			((RestaurantsMapsFragment) mapFragment).showPlace(place);
	}

	public Location getCurrentLocation() {
		if (this.currentLocation == null) {
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
			this.currentLocation = new Location(this.provider);
			this.currentLocation.setLatitude(settings.getFloat(LATITUDE, 0));
			this.currentLocation.setLongitude(settings.getFloat(LONGITUDE, 0));
			if (this.currentLocation.getLatitude() == 0)
				this.currentLocation = null;
		}
		return this.currentLocation;
	}

	private void setCurrentLocation(Location location) {
		this.currentLocation = location;
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putFloat(LATITUDE, (float) location.getLatitude());
		editor.putFloat(LONGITUDE, (float) location.getLongitude());
		editor.commit();
	}

	protected boolean isBetterLocation(Location location,
			Location currentBestLocation) {
		if (currentBestLocation == null) {
			return true;
		}

		long timeDelta = location.getTime() - currentBestLocation.getTime();
		boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
		boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
		boolean isNewer = timeDelta > 0;

		if (isSignificantlyNewer) {
			return true;
		} else if (isSignificantlyOlder) {
			return false;
		}

		int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation
				.getAccuracy());
		boolean isLessAccurate = accuracyDelta > 0;
		boolean isMoreAccurate = accuracyDelta < 0;
		boolean isSignificantlyLessAccurate = accuracyDelta > 200;

		boolean isFromSameProvider = isSameProvider(location.getProvider(),
				currentBestLocation.getProvider());

		if (isMoreAccurate) {
			return true;
		} else if (isNewer && !isLessAccurate) {
			return true;
		} else if (isNewer && !isSignificantlyLessAccurate
				&& isFromSameProvider) {
			return true;
		}
		return false;
	}

	private boolean isSameProvider(String provider1, String provider2) {
		if (provider1 == null) {
			return provider2 == null;
		}
		return provider1.equals(provider2);
	}
}
