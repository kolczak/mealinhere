package com.kolczak.mealinhere.main;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kolczak.mealinhere.common.ILocationProvider;
import com.kolczak.mealinhere.common.IPlacesFragment;
import com.kolczak.mealinhere.common.IPlacesProvider;
import com.kolczak.mealinhere.common.Place;

public class RestaurantsMapsFragment extends SupportMapFragment implements
		IPlacesFragment {

	private List<Marker> markersList = new ArrayList<Marker>();

	public RestaurantsMapsFragment() {
	}

	@Override
	public void onResume() {
		super.onResume();

		Activity parentActivity = getActivity();
		if (parentActivity instanceof IPlacesProvider) {
			List<Place> places = ((IPlacesProvider) parentActivity).getPlaces();
			if (places != null)
				refresh(places);
		}
		if (parentActivity instanceof ILocationProvider) {
			Location currentLocation = ((ILocationProvider) parentActivity)
					.getCurrentLocation();
			if (currentLocation != null)
				showLocation(currentLocation, true);
		}
	}

	public void showLocation(Location location, boolean animated) {
		LatLng latLng = new LatLng(location.getLatitude(),
				location.getLongitude());
		showLocation(latLng, animated);
	}

	public void showLocation(LatLng location, boolean animated) {
		GoogleMap map = getMap();
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 15));

		if (animated)
			map.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
	}

	public void showPlace(Place place) {
		for (Marker marker : this.markersList) {
			if (marker.getTitle().equals(place.getName())) {
				marker.showInfoWindow();
				showLocation(marker.getPosition(), true);
				break;
			}
		}
	}

	@Override
	public void refresh(List<Place> data) {
		GoogleMap map = getMap();

		this.markersList.clear();
		Marker marker;
		for (Place place : data) {
			LatLng position = new LatLng(place.getLatitude(),
					place.getLongitude());
			marker = map.addMarker(new MarkerOptions().position(position)
					.title(place.getName()).snippet(place.getVicinity()));
			this.markersList.add(marker);
			Log.d("icon: ", place.getIcon());
		}
	}

	@Override
	public void showProgress() {
	}
}
