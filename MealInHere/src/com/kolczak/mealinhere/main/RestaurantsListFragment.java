package com.kolczak.mealinhere.main;

import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kolczak.mealinhere.R;
import com.kolczak.mealinhere.common.IPlacesFragment;
import com.kolczak.mealinhere.common.IPlacesProvider;
import com.kolczak.mealinhere.common.Place;

/**
 * A dummy fragment representing a section of the app, but that simply displays
 * dummy text.
 */
public class RestaurantsListFragment extends ListFragment implements IPlacesFragment {
	/**
	 * The fragment argument representing the section number for this fragment.
	 */
	public static final String TEXT = "restaurants list";
	private View progressBar;
	private RestaurantsListAdapter adapter;

	public RestaurantsListFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.restaurants_list_fragment,
				container, false);

		this.progressBar = (View) rootView
				.findViewById(R.id.restaurants_list_fragment_progressbar);

		return rootView;
	}

	@Override
	public void onResume ()
	{
		super.onResume();
		Activity parentActivity = getActivity();
		if (parentActivity instanceof IPlacesProvider)
		{
			List<Place> places = ((IPlacesProvider)parentActivity).getPlaces();
			if (places != null)
				refresh(places);
		}
	}
	
	public void showProgress() {
		this.progressBar.setVisibility(View.VISIBLE);
	}

	@Override
	public void refresh(List<Place> placesList) {
		this.progressBar.setVisibility(View.GONE);
		if (this.adapter == null) {
			this.adapter = new RestaurantsListAdapter(getActivity(), placesList);
			this.setListAdapter(this.adapter);
		} else {
			this.adapter.refill(placesList);
		}
	}
}