package com.kolczak.mealinhere.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.kolczak.mealinhere.common.Place;

public class PlacesDataSource {
	// Database fields
	private SQLiteDatabase database;
	private MySqlHelper dbHelper;
	private String[] allColumns = { MySqlHelper.COLUMN_ID,
			MySqlHelper.COLUMN_ICON, MySqlHelper.COLUMN_NAME,
			MySqlHelper.COLUMN_VICINITY, MySqlHelper.COLUMN_LATITUDE,
			MySqlHelper.COLUMN_LONGITUDE };

	public PlacesDataSource(Context context) {
		this.dbHelper = new MySqlHelper(context);
	}

	public void open() throws SQLException {
		this.database = this.dbHelper.getWritableDatabase();
	}

	public void close() {
		this.dbHelper.close();
	}

	public void setAllPlaces(List<Place> places) {
		this.database.delete(MySqlHelper.TABLE_PLACES, null, null);
		for (Place place : places)
			createPlace(place);
	}

	private void createPlace(Place place) {
		ContentValues values = new ContentValues();
		values.put(MySqlHelper.COLUMN_ICON, place.getIcon());
		values.put(MySqlHelper.COLUMN_NAME, place.getName());
		values.put(MySqlHelper.COLUMN_VICINITY, place.getVicinity());
		values.put(MySqlHelper.COLUMN_LATITUDE, place.getLatitude());
		values.put(MySqlHelper.COLUMN_LONGITUDE, place.getLongitude());
		this.database.insert(MySqlHelper.TABLE_PLACES, null,
				values);
	}

	public List<Place> getAllPlaces() {
		List<Place> places = new ArrayList<Place>();

		Cursor cursor = this.database.query(MySqlHelper.TABLE_PLACES, this.allColumns,
				null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Place place = cursorToPlace(cursor);
			places.add(place);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		return places;
	}

	private Place cursorToPlace(Cursor cursor) {
		Place place = new Place();
		place.setIcon(cursor.getString(1));
		place.setName(cursor.getString(2));
		place.setVicinity(cursor.getString(3));
		place.setLatitude(cursor.getDouble(4));
		place.setLongitude(cursor.getDouble(5));
		return place;
	}
}
