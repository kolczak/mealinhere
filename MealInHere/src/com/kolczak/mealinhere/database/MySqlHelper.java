package com.kolczak.mealinhere.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySqlHelper extends SQLiteOpenHelper {

	  public MySqlHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
	}

	public static final String TABLE_PLACES = "places";
	  public static final String COLUMN_ID = "_id";
	  public static final String COLUMN_ICON = "icon";
	  public static final String COLUMN_NAME = "name";
	  public static final String COLUMN_VICINITY = "vicinity";
	  public static final String COLUMN_LATITUDE = "latitude";
	  public static final String COLUMN_LONGITUDE = "longitude";

	  private static final String DATABASE_NAME = "places.db";
	  private static final int DATABASE_VERSION = 1;

	  private static final String DATABASE_CREATE = "create table "
	      + TABLE_PLACES + "(" + COLUMN_ID
	      + " integer primary key autoincrement, " 
	      + COLUMN_ICON + " text, "
	      + COLUMN_NAME + " text, "
	      + COLUMN_VICINITY + " text, "
	      + COLUMN_LATITUDE + " real, "
	      + COLUMN_LONGITUDE + " real "
	      +");" ;

	  public MySqlHelper(Context context) {
	    super(context, DATABASE_NAME, null, DATABASE_VERSION);
	  }

	  @Override
	  public void onCreate(SQLiteDatabase database) {
	    database.execSQL(DATABASE_CREATE);
	  }

	  @Override
	  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	    Log.w(MySqlHelper.class.getName(),
	        "Upgrading database from version " + oldVersion + " to "
	            + newVersion + ", which will destroy all old data");
	    db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLACES);
	    onCreate(db);
	  }

}
