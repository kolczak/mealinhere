package com.kolczak.mealinhere.dataexchange;

import java.util.List;
import java.util.Locale;

import org.json.JSONObject;

import com.kolczak.mealinhere.common.Constants;
import com.kolczak.mealinhere.common.Place;

public class PlacesLoader extends BasicDataLoader {

	private static final String REQUEST_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%f,%f&radius=1000&types=food&sensor=true&key=" + Constants.API_KEY;
	private double latitude;
	private double longitude;
	private List<Place> placesList;
	
	public PlacesLoader(IResponseHandler responseHandler, double latitude, double longitude) {
		super(responseHandler);
		this.latitude = latitude;
		this.longitude = longitude;
	}

	@Override
	protected String getRequestUrl() {
		String url = String.format(Locale.US, REQUEST_URL, this.latitude, this.longitude);
		return url;
	}

	@Override
	protected void handleResponse(JSONObject jsonObject) {
		this.placesList = PlacesParser.getPlaces(jsonObject);
	}

	@Override
	public List<Place> getResults() {
		return this.placesList;
	}
}