package com.kolczak.mealinhere.dataexchange;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONObject;

import android.os.AsyncTask;

public abstract class BasicDataLoader extends AsyncTask<Void, Void, JSONObject> {

	private IResponseHandler responseHandler;

	protected abstract String getRequestUrl();
	protected abstract void handleResponse(JSONObject jsonObject);
	public abstract List<? extends Object> getResults();
	
	public BasicDataLoader(IResponseHandler responseHandler) {
		this.responseHandler = responseHandler;
	}

	@Override
	protected void onPostExecute(JSONObject result) {
		super.onPostExecute(result);
		if (result != null)
		{
			handleResponse(result);
			this.responseHandler.onDataExchanged(this);
		}
		else
			this.responseHandler.onDataExchangedError(this);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected JSONObject doInBackground(Void... urls) {
		JSONObject jsonResult = null;

		HttpURLConnection conn = null;
	    try {
	        URL url = new URL(getRequestUrl());
	        conn = (HttpURLConnection) url.openConnection();
	        InputStream in = conn.getInputStream();

	        jsonResult = new JSONObject(convertStreamToString(in));
	    } catch (Exception e) {
			Logger.getLogger(BasicDataLoader.class.getName()).log(Level.SEVERE, null, e);
			jsonResult = null;
		} finally {
	        if (conn != null) {
	            conn.disconnect();
	        }
	    }

		return jsonResult;
	}

	private String convertStreamToString(InputStream in) {
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		StringBuilder jsonstr = new StringBuilder();
		String line;
		try {
			while ((line = br.readLine()) != null) {
				String t = line + "\n";
				jsonstr.append(t);
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonstr.toString();
	}

}
