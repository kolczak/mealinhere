package com.kolczak.mealinhere.dataexchange;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.kolczak.mealinhere.common.Place;

public class PlacesParser {

	public static Place getPlace(JSONObject jsonObject) {
		Place place = new Place();
		try {
			JSONObject geometry = (JSONObject) jsonObject.get("geometry");
			JSONObject location = (JSONObject) geometry.get("location");
			place.setLatitude((Double) location.get("lat"));
			place.setLongitude((Double) location.get("lng"));
			place.setIcon(jsonObject.getString("icon"));
			place.setName(jsonObject.getString("name"));
			place.setVicinity(jsonObject.getString("vicinity"));
			place.setId(jsonObject.getString("id"));
		} catch (JSONException e) {
			Logger.getLogger(PlacesParser.class.getName()).log(Level.SEVERE, null, e);
			place = null;
		}
		return place;
	}

	public static List<Place> getPlaces(JSONObject jsonObject) {
		List<Place> places = new ArrayList<Place>();

		try {
			if (!jsonObject.isNull("results")) {
				JSONArray placesArray = jsonObject.getJSONArray("results");
				for (int i = 0; i < placesArray.length(); i++) {
					JSONObject placeObject = placesArray.getJSONObject(i);
					places.add(getPlace(placeObject));
				}
			}
			
		} catch (Exception e) {
			Logger.getLogger(Place.class.getName()).log(Level.SEVERE, null, e);
		}

		return places;
	}
}
