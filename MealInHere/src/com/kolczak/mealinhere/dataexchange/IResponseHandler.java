package com.kolczak.mealinhere.dataexchange;


public interface IResponseHandler {
	void onDataExchanged(BasicDataLoader dataLoader);
	void onDataExchangedError(BasicDataLoader dataLoader);
}
